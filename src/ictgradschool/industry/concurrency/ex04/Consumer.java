package ictgradschool.industry.concurrency.ex04;

import java.util.concurrent.BlockingQueue;

public class Consumer implements Runnable{
    private final BankAccount account;
    private final BlockingQueue<Transaction> que;

    public Consumer(BankAccount account,BlockingQueue que){
        this.account=account;
        this.que=que;
    }

    @SuppressWarnings("Duplicates")
    @Override
    public void run() {
        // check if thread has been interrupted
        try {
            while(!que.isEmpty()||!Thread.currentThread().isInterrupted()) {
                Transaction transaction = que.take();

                switch (transaction._type) {
                    case Deposit:
                        account.deposit(transaction._amountInCents);
                        break;
                    case Withdraw:
                        account.withdraw(transaction._amountInCents);
                        break;
                }
            }
        } catch (InterruptedException e) {

            Thread.currentThread().interrupt();
        }
    }
}


