package ictgradschool.industry.concurrency.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

public class ConcurrencyBankingApp {

    public static void main(String[] args) {
        final BlockingQueue<Transaction> queue = new ArrayBlockingQueue<>(10);
        // Acquire Transactions to process.
        final List<Transaction> transactions = TransactionGenerator.readDataFile();
        BankAccount account = new BankAccount();
        List<Thread> threads = new ArrayList<>();


        Thread consumer_1 = new Thread(new Consumer(account, queue));
        Thread consumer_2 = new Thread(new Consumer(account, queue));


        Thread producer = new Thread(new Runnable() {
            @Override
            public void run() {

                for (Transaction transaction : transactions) {
                    try {
                        queue.put(transaction);
                    } catch (InterruptedException e) {

                        e.printStackTrace();
                    }
                }
            }

        });

//Starts the producer thread and adds customers
        producer.start();
        threads.add(consumer_1);
        threads.add(consumer_2);
//start consumer threads
        for (Thread thread : threads) {
            thread.start();
        }

//join the threads
        try {
            producer.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

//gracefully terminate the consumers - Darcy helped with this bit
        for (Thread thread : threads) {
            thread.interrupt();
        }

//see if threads have stopped.
        for (Thread thread : threads
        ) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        System.out.println("Final balance: " + account.getFormattedBalance());

    }

}
