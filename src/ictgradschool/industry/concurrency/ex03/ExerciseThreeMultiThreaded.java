package ictgradschool.industry.concurrency.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class ExerciseThreeMultiThreaded extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    @Override
    protected double estimatePI(long numSamples) throws InterruptedException {
        // TODO Implement this.

        ThreadLocalRandom threadLocalRandom = ThreadLocalRandom.current();

        List<Thread> threads = new ArrayList<>();

        int numberOfThreads = 20;

        class StoredVal {
            public long getNumInC() {
                return numInC;
            }

            public synchronized void setNumInC(long numInC) {
                this.numInC += numInC;
            }

            long numInC = 0;
        }

        StoredVal store = new StoredVal();


        Runnable subUnit = () -> {
            ThreadLocalRandom threadLocalRandom1 = ThreadLocalRandom.current();

            long numInsideCircle = 0;

            for (long i = 0; i < (numSamples / numberOfThreads); i++) {


                double x = threadLocalRandom.nextDouble();
                double y = threadLocalRandom.nextDouble();

                if ((x * x) + (y * y) < 1.0) {
                    numInsideCircle++;
                }
            }
            store.setNumInC(numInsideCircle);
        };


        for (int i = 0; i < numberOfThreads; i++) {
            threads.add(new Thread(subUnit));
        }

        for (Thread thread : threads
        ) {
            thread.start();
        }
        for (Thread thread : threads
        ) {
            thread.join();
        }


        return (4.0 * (double) store.getNumInC()) / (double) (numSamples / numberOfThreads * numberOfThreads);
    }

    /**
     * Program entry point.
     */
    public static void main(String[] args) throws InterruptedException {
        new ExerciseThreeMultiThreaded().start();
    }
}



