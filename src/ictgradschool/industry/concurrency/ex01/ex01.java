package ictgradschool.industry.concurrency.ex01;

/**
 * A simple Runnable that will continually increment the counter until its thread is interrupted.
 */
public class ex01 {

    private void start() throws InterruptedException {
        System.out.println("Creating myRunnable");


        Runnable myRunnable = new Runnable() {
            @Override
            public void run() {
                for (int i = 0; i <= 1_000_000; i++) {
                    System.out.println(i);
                    if (Thread.currentThread().isInterrupted()) {

                        return;
                    }

                }


            }
        };

        Thread myThread = new Thread(myRunnable);
        myThread.start();
        System.out.println("Starting myRunnable");

/*        We do this part (Thread.sleep) so that it actually displays something to the console for us.
*/
        Thread.sleep(500);

/*        Here we are interrupting it before it gets to a million... randomly interrupted. Will be different every time.

 */
        myThread.interrupt();


    }

    public static void main(String[] args) throws InterruptedException {
        (new ex01()).start();
    }
}


