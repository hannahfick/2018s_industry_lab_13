package ictgradschool.industry.concurrency.ex05;

import java.util.ArrayList;
import java.util.List;

public class PrimeFactorsTask implements Runnable {

    public enum TaskState{Initialised, Completed, Aborted};
    private long N;
    private List<Long> primes;
    public TaskState state;

    public PrimeFactorsTask(long n){
        this.N=n;
        primes=new ArrayList<>();
        state=TaskState.Initialised;
    }


    @Override
    public void run() {
        //do the algorithm and store results in primes list.
        //copied and pasted code from:https://introcs.cs.princeton.edu/java/13flow/Factors.java.html
        // command-line argument


        System.out.print("The prime factorization of " + N + " is: ");

        //should there be a try/catch? what happens if thread interupted?
        // intellij doesn't seem to think there will be an exception here.
//        try {

        // for each potential factor
        for (long factor = 2; factor * factor <= N; factor++) {
            if (Thread.currentThread().isInterrupted()){
                state=TaskState.Aborted;
                return;
            }
            // if factor is a factor of n, repeatedly divide it out
            while (N % factor == 0&&!Thread.interrupted()) {
                //add factors to list
                primes.add(factor);
//                        System.out.print(factor + " ");
                N = N / factor;
            }

        }

        // if biggest factor occurs only once, n > 1
        if (N > 1) primes.add(N);
//                else System.out.println();

        //fin running so set state to completed
        state=TaskState.Completed;
    }


    public long n(){
        return N;
    }

    public List<Long> getPrimeFactors() throws IllegalStateException{
        if(state!=TaskState.Completed){
            throw new IllegalStateException();

        }
        return primes;
    }


    public TaskState getState(){
        return state;
    }

}
